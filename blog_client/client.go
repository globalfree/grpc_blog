package main

import (
	"context"
	"fmt"
	"jimmy/grpc-go/blog/blogpb"
	"log"

	"google.golang.org/grpc"
)

type blogItemReq struct {
	AuthorID string `bson:"author_id"`
	Content  string `bson:"content"`
	Title    string `bson:"title"`
}

func insertBlog(req *blogItemReq) string {
	opts := grpc.WithInsecure()

	// cc, err := grpc.Dial("192.168.99.100:50051", opts)
	cc, err := grpc.Dial("localhost:50051", opts)
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close()

	fmt.Println("creating the blog")
	c := blogpb.NewBlogServiceClient(cc)
	blog := &blogpb.Blog{
		AuthorId: req.AuthorID,
		Title:    req.Title,
		Content:  req.Content,
	}
	CreateBlogRes, err := c.CreateBlog(context.Background(), &blogpb.CreateBlogRequest{Blog: blog})
	if err != nil {
		log.Fatalf("unexpected error: %v \n", err)
	}
	fmt.Printf("blog has ben created: %v \n", CreateBlogRes)

	blogID := CreateBlogRes.GetBlog().GetId()

	return blogID
}

func main() {
	fmt.Println("blog client")

	// blogID := CreateBlogRes.GetBlog().GetId()

	data := &blogItemReq{
		AuthorID: "jim",
		Title:    "1st blog",
		Content:  "1st blog content",
	}

	insertBlog(data)

	//read
	// fmt.Printf("Reading the blog \n")

	// _, err2 := c.ReadBlog(context.Background(), &blogpb.ReadBlogRequest{BlogId: "5c35a7562e767bfbb29d000a"})
	// if err2 != nil {
	// 	fmt.Printf("Error happened while reading: %v \n", err2)
	// }

	// readBlogReq := &blogpb.ReadBlogRequest{BlogId: blogID}
	// readBlogRes, readBlogErr := c.ReadBlog(context.Background(), readBlogReq)
	// if readBlogErr != nil {
	// 	fmt.Printf("Error happened while reading: %v \n", readBlogErr)
	// }

	// fmt.Printf("Blog was read: %v \n", readBlogRes)

	// //update blog
	// newBlog := &blogpb.Blog{
	// 	Id:       blogID,
	// 	AuthorId: "change Author",
	// 	Title:    "my first blog (edited)",
	// 	Content:  "awsome edited",
	// }
	// updRes, updErr := c.UpdateBlog(context.Background(), &blogpb.UpdateBlogRequest{Blog: newBlog})
	// if updErr != nil {
	// 	fmt.Printf("error happened while updating: %v \n", updErr)
	// }
	// fmt.Printf("blog was updated: %v \n", updRes)

	// // delete blog
	// delRes, delErr := c.DeleteBlog(context.Background(), &blogpb.DeleteBlogRequest{BlogId: blogID})
	// if delErr != nil {
	// 	fmt.Printf("error happened while deleting: %v \n", delErr)
	// }
	// fmt.Printf("blog was deleted: %v \n", delRes)

	// // list blog

	// req := &blogpb.ListBlogRequest{}
	// stream, err := c.ListBlog(context.Background(), req)
	// if err != nil {
	// 	log.Fatalf("error while calling PrimeNumDecomposition RPC: %v", err)
	// }

	// for {
	// 	res, err := stream.Recv()
	// 	if err == io.EOF {
	// 		break
	// 	}
	// 	if err != nil {
	// 		log.Fatalf("Something happened: %v", err)
	// 	}
	// 	log.Println(res.GetBlog())
	// }

}
