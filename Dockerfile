FROM golang:1.11

LABEL maintainer "jimmy"

# Set Environment variables
ENV appDir /go/src/jimmy/grpc-go/blog/
ENV appMain /var/www/app/main

RUN mkdir -p ${appDir}
ADD . ${appDir}
WORKDIR ${appDir}/blog_server

RUN go get github.com/mongodb/mongo-go-driver/bson
RUN go get google.golang.org/grpc
RUN go build


CMD ["go", "run", "server.go"]

EXPOSE 50051 
